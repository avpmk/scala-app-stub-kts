plugins {
    application
    scala
    id("cz.alenkacz.gradle.scalafmt") version "1.14.0"
    id("com.github.johnrengelman.shadow") version "6.0.0"
}

val fullScalaVersion = "2.13.3"
val binaryScalaVersion = fullScalaVersion.substringBeforeLast('.')

// current maximum for scala 2.13.3
val javaVersion = JavaVersion.VERSION_12

for (task in listOf(tasks.compileScala, tasks.compileTestScala)) {
    task {
        scalaCompileOptions.additionalParameters = listOf(
                "-target:$javaVersion",
                "-feature",
                "-language:implicitConversions",
                "-Xlint",
                "-Xfatal-warnings",
                "-Ymacro-annotations"
        )
    }
}


application.mainClassName = "Program"
tasks.jar {
    manifest.attributes["Main-Class"] = application.mainClassName
}


//tasks.compileScala { dependsOn(tasks.scalafmt) }
//tasks.compileTestScala { dependsOn(tasks.testScalafmt) }
tasks.compileScala { finalizedBy(tasks.checkScalafmt) }
tasks.compileTestScala { finalizedBy(tasks.checkTestScalafmt) }


tasks.shadowJar {
    val appending = com.github.jengelman.gradle.plugins.shadow.transformers.AppendingTransformer::class.java
    for (fileToAppend in listOf("reference.conf", "version.conf")) {
        transform(appending) {
            resource = fileToAppend
        }
    }
}


repositories {
    mavenCentral()
}


typealias DepFn = (String, String, String) -> Unit

class Dep(private val fn: DepFn) : DepFn by fn {
    operator fun invoke(group: String, version: String, names: Iterable<String>) {
        for (name in names) {
            fn(group, name, version)
        }
    }

    infix fun mapSecond(f: (String) -> String) = Dep { a, b, c ->
        fn(a, f(b), c)
    }
}

fun scalaArtifact(base: Dep): Dep = base mapSecond { it + "_" + binaryScalaVersion }


dependencies {
    val javaLib = Dep { a, b, c -> implementation(a, b, c) }
    val javaTestLib = Dep { a, b, c -> testImplementation(a, b, c) }
    val scalaLib = scalaArtifact(javaLib)
    val scalaTestLib = scalaArtifact(javaTestLib)

    implementation("org.scala-lang", "scala-library", fullScalaVersion)

}
